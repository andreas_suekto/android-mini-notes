package com.simplehimmel.mininotes.controller.note;

import java.util.ArrayList;
import java.util.UUID;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;

import com.simplehimmel.mininotes.R;
import com.simplehimmel.mininotes.model.Note;
import com.simplehimmel.mininotes.model.NoteWarehouse;

public class NotePagerActivity extends ActionBarActivity {
	private ViewPager mViewPager;
	private ArrayList<Note> mNotes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Retrieve notes
		mNotes = NoteWarehouse.get(this).getNotes();
				
		// Set viewPager as this activity view;
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.viewPager);
		setContentView(mViewPager);
		
		// set-up viewPager
		setupViewPager();
		
		UUID id = (UUID)getIntent().getSerializableExtra(NoteFragment.EXTRA_NOTE_ID);
		int index = -1;
		for (int i = 0; i < mNotes.size(); i++) {
			if (mNotes.get(i).getId().equals(id)) {
				index = i;
				break;
			}
		}
		mViewPager.setCurrentItem(index);
		setTitle(mNotes.get(index).getTitle());
	}

	private void setupViewPager() {
		if (mViewPager == null) {
			return;
		}
		
		// Setting the adapter
		FragmentManager fm = getSupportFragmentManager();
		mViewPager.setAdapter(new FragmentPagerAdapter(fm) {
			@Override
			public int getCount() {
				return mNotes.size();
			}
			
			@Override
			public Fragment getItem(int position) {
				return NoteFragment.newInstance(mNotes.get(position).getId());
			}
		});
		
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int pos) {
				Note note = mNotes.get(pos);
				if (note.getTitle() != null) {
					setTitle(note.getTitle());	
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// Empty
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// Empty
			}
		});
	}
}
