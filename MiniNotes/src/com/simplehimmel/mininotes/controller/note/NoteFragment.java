package com.simplehimmel.mininotes.controller.note;

import java.util.UUID;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.simplehimmel.mininotes.R;
import com.simplehimmel.mininotes.model.Note;
import com.simplehimmel.mininotes.model.Note.Color;
import com.simplehimmel.mininotes.model.NoteWarehouse;

public class NoteFragment extends Fragment {
	public static final String  EXTRA_NOTE_ID =
				"com.simplehimmel.mininotes.note_id";
	
	private Note mNote;
	
	private EditText mTitleEditText;
	
	private EditText mContentEditText;
	private View mContentBorder;
	
	private Button mPinkColorButton;
	private Button mYellowColorButton;
	private Button mGreenColorButton;
	
	// Instantiate a note with specific id
	// put in fragment's arguments provide loose coupling with activity
	public static NoteFragment newInstance(UUID id) {
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_NOTE_ID, id);
		
		NoteFragment fragment = new NoteFragment();
		fragment.setArguments(args);
		
		return fragment;
	}
	
	/*
	 * Life Cycle Section
	 */
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		
		UUID id = (UUID)getArguments().getSerializable(EXTRA_NOTE_ID);
		mNote = NoteWarehouse.get(getActivity()).getNote(id);
	}
	
	@TargetApi(11)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, 
			Bundle savedInstanceState) 
	{
		View v = inflater.inflate(R.layout.fragment_note, parent, false);
		
		// Up Button Setup
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			if (NavUtils.getParentActivityName(getActivity()) != null) {
				getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
			}
		}
		
		// Get Reference to the title
		mTitleEditText = (EditText)v.findViewById(R.id.title_editText);
		mTitleEditText.setText(mNote.getTitle());
		setupTitleEditText();
		
		// Get Reference to the content
		mContentEditText = (EditText)v.findViewById(R.id.content_editText);
		mContentBorder = v.findViewById(R.id.content_border);
		setNoteViewColor(mNote.getColor());
		mContentEditText.setText(mNote.getContent());
		setupContentEditText();
		
		// Get reference to the buttons
		mPinkColorButton = (Button)v.findViewById(R.id.pink_color_button);
		mYellowColorButton = (Button)v.findViewById(R.id.yellow_color_button);
		mGreenColorButton = (Button)v.findViewById(R.id.green_color_button);
		setupColorButton();
		
		return v;
	}
	
	
	/*
	 * Options Menu Section
	 */
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_note_options, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		switch (item.getItemId()) {
			// Up Button Pressed
			case android.R.id.home:
				if (NavUtils.getParentActivityName(getActivity()) != null) {
					NavUtils.navigateUpFromSameTask(getActivity());
				}
				return true;
				
			// Delete note pressed
			case R.id.menu_delete_note:
				NoteWarehouse warehouse = NoteWarehouse.get(getActivity());
				// remove crime from list
				warehouse.removeNote(mNote);
				// return to list
				if (NavUtils.getParentActivityName(getActivity()) != null) {
					NavUtils.navigateUpFromSameTask(getActivity());
				}
				return true;
				
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	/*
	 * Helper Functions
	 */
	private void setupTitleEditText() {
		if (mTitleEditText == null) {
			return;
		}
		
		mTitleEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				mNote.setTitle(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// empty
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// empty
			}
		});
	}
	
	private void setupContentEditText() {
		if (mContentEditText == null) {
			return;
		}
		
		mContentEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				mNote.setContent(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// empty
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// empty
			}
		});
	}
	
	private void setupColorButton() {
		if (mPinkColorButton == null || mYellowColorButton == null 
				|| mGreenColorButton == null ) {
			return;
		}
		
		mPinkColorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNote.setColor(Color.PINK);
				setNoteViewColor(mNote.getColor());
			}
		});
		
		mYellowColorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNote.setColor(Color.YELLOW);
				setNoteViewColor(mNote.getColor());
			}
		});
		
		mGreenColorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNote.setColor(Color.GREEN);
				setNoteViewColor(mNote.getColor());
			}
		});
	}
	
	private void setNoteViewColor(Note.Color color) {
		if (mContentEditText == null || mContentBorder == null) {
			return;
		}
		
		int contentColor;
		
		switch (color) {
			case PINK:
				contentColor = R.drawable.pink_light;
				break;
			case YELLOW:
				contentColor = R.drawable.yellow_light;
				break;
			case GREEN:
				contentColor = R.drawable.green_light;
				break;
			default:
				contentColor = R.drawable.yellow_light;
				break;
		}
		
		mContentEditText.setBackgroundResource(contentColor);
	}
}
