package com.simplehimmel.mininotes.controller.list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.simplehimmel.mininotes.R;
import com.simplehimmel.mininotes.controller.note.NoteFragment;
import com.simplehimmel.mininotes.controller.note.NotePagerActivity;
import com.simplehimmel.mininotes.model.Note;
import com.simplehimmel.mininotes.model.NoteWarehouse;
import com.simplehimmel.mininotes.model.util.NoteUtil;

public class NoteListFragment extends ListFragment {
	private final static int MAX_LENGTH_TITLE_PREVIEW = 24;
	private final static int MAX_LENGTH_SUMMARY_PREVIEW = 135;
	
	private ArrayList<Note> mNotes;
	private View mLoadingContainer; // Animated view during the change of activity
	
	/*
	 * Life Cycle Section
	 */
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true); // Make sure this fragment has options menu
		
		// Remove the notes without any content & title
		cleanNoteWarehouse();
		
		// Cache the notes which is represented by this list view
		getActivity().setTitle(R.string.mini_notes_title);
		mNotes = NoteWarehouse.get(getActivity()).getNotes();
		
		NoteListAdapter adapter = new NoteListAdapter(mNotes);
		setListAdapter(adapter);
	}
	
	@TargetApi(11)
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup parent,
								Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_note_list, parent, false);
		
		mLoadingContainer = v.findViewById(R.id.note_list_loading_container);
		mLoadingContainer.setVisibility(View.INVISIBLE);
		
		// Create new note at click of empty list's text
		TextView emptyTextView = (TextView)v.findViewById(R.id.empty_add_note_textView);
		emptyTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mLoadingContainer.setVisibility(View.VISIBLE);
				addNewNote();
			}
		});
		
		// Installing context menu to list view
		ListView listView = (ListView)v.findViewById(android.R.id.list);
		setupListViewGracefullFallBack(listView);
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// Making sure to refresh the view with the latest data.
		NoteWarehouse.get(getActivity()).notifyDataSetChange();
		((NoteListAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// Save the data to file on every pause, the safest time to safe.
		NoteWarehouse.get(getActivity()).save();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Note note = ((NoteListAdapter)getListAdapter()).getItem(position);
		
		// Start the note'details activity
		startNoteActivity(note);
	}
	
	/*
	 * Option Menu Section
	 */
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_note_list_options, menu);
		
		MenuItem sortMenuItem 
			= (MenuItem)menu.findItem(R.id.menu_item_sort_date_note);
		setOptionTitleForNextSortDate(sortMenuItem);
		
		sortMenuItem 
			= (MenuItem)menu.findItem(R.id.menu_item_sort_color_note);
		setOptionTitleForNextSortColor(sortMenuItem);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_item_add_note:
				addNewNote();
				return true;
			case R.id.menu_item_sort_date_note:
				if (item.getTitle().equals(
						getString(R.string.note_list_sort_date_note_asc))) 
				{
					NoteWarehouse.get(getActivity())
								 .sortNoteByDate(NoteUtil.SORT_ASCENDING);
				}
				else {
					NoteWarehouse.get(getActivity())
								 .sortNoteByDate(NoteUtil.SORT_DESCENDING);
				}
				setOptionTitleForNextSortDate(item);
				((NoteListAdapter)getListAdapter()).notifyDataSetChanged();
				return true;
			case R.id.menu_item_sort_color_note:
				if (item.getTitle().equals(
						getString(R.string.note_list_sort_color_note_pink))) 
				{
					NoteWarehouse.get(getActivity())
								 .sortNoteByColor(Note.Color.PINK);
				}
				else if (item.getTitle().equals(
						getString(R.string.note_list_sort_color_note_green))) 
				{
					NoteWarehouse.get(getActivity())
					 			 .sortNoteByColor(Note.Color.GREEN);					
				}
				else if (item.getTitle().equals(
						getString(R.string.note_list_sort_color_note_yellow))) 
				{
					NoteWarehouse.get(getActivity())
					 			 .sortNoteByColor(Note.Color.YELLOW);
				}
				else {
					NoteWarehouse.get(getActivity())
					 			.sortNoteByColor(null);
				}
				setOptionTitleForNextSortColor(item);
				((NoteListAdapter)getListAdapter()).notifyDataSetChanged();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Setting option menu item title, to reflect
	 * the next color sorting effect of it.
	 * 
	 * @param sortMenuItem
	 */
	private void setOptionTitleForNextSortColor(MenuItem sortMenuItem) {
		NoteWarehouse warehouse = NoteWarehouse.get(getActivity());
		if (warehouse.getSortPrioritizedColor() == null) {
			sortMenuItem.setTitle(
					R.string.note_list_sort_color_note_pink);
		}
		else {
			switch (warehouse.getSortPrioritizedColor()) {
				case PINK:
					sortMenuItem.setTitle(
							R.string.note_list_sort_color_note_green);
					break;
				case GREEN:
					sortMenuItem.setTitle(
							R.string.note_list_sort_color_note_yellow);
					break;
				case YELLOW:
					sortMenuItem.setTitle(
							R.string.note_list_sort_color_note_none);
					break;
				default:
					sortMenuItem.setTitle(
							R.string.note_list_sort_color_note_pink);
					break;
			}
		}
	}
	
	/**
	 * Set the option menu item title to
	 * reflect the date sorting mode at the next press.
	 * @param sortMenuItem
	 */
	private void setOptionTitleForNextSortDate(MenuItem sortMenuItem) {
		NoteWarehouse warehouse = NoteWarehouse.get(getActivity());
		if (warehouse.isSortDateAscendingMode()) {
			sortMenuItem.setTitle(R.string.note_list_sort_date_note_desc);
		}
		else {
			sortMenuItem.setTitle(R.string.note_list_sort_date_note_asc);
		}
	}
	
	/*
	 * Context Menu Section
	 */
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo info) {
		getActivity().getMenuInflater().inflate(R.menu.list_item_note_context, menu);
		
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// Floating menu for old OS
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		NoteListAdapter adapter = (NoteListAdapter)getListAdapter();
		Note note = adapter.getItem(position);
		
		switch(item.getItemId()) {
			case R.id.menu_item_delete_note:
				NoteWarehouse.get(getActivity()).removeNote(note);
				adapter.notifyDataSetChanged();
				return true;
		}
		
		return super.onContextItemSelected(item);
	}
	
	/*
	 * Helper Functions
	 */
	/**
	 * Remove all {@link Note}s which does not has
	 * content & title.
	 */
	private void cleanNoteWarehouse() {
		NoteWarehouse warehouse = NoteWarehouse.get(getActivity());
		Set<Note> toBeRemoved = new HashSet<Note>();
		for (Note note : warehouse.getNotes()) {
			if ((note.getTitle() == null  || note.getTitle().trim().length() == 0)
					&& (note.getContent() == null || note.getContent().trim().length() == 0)) 
			{
				toBeRemoved.add(note);
			}
		}
		
		for (Note note : toBeRemoved) {
			warehouse.removeNote(note);
		}
	}
	
	@TargetApi(11)
	private void setupListViewGracefullFallBack(ListView listView) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			// Context floating menu
			registerForContextMenu(listView);
		}
		else {
			// Contextual action bar
			listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {
				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					// TODO Auto-generated method stub
					return false;
				}
				
				@Override
				public void onDestroyActionMode(ActionMode mode) {
					// TODO Auto-generated method stub
				}
				
				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					MenuInflater inflater = mode.getMenuInflater();
					inflater.inflate(R.menu.list_item_note_context, menu);
					return true;
				}
				
				@Override
				public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
					switch (item.getItemId()) {
						case R.id.menu_item_delete_note:
							NoteListAdapter adapter = (NoteListAdapter)getListAdapter();
							NoteWarehouse noteWarehouse = NoteWarehouse.get(getActivity());
							for (int i = adapter.getCount() - 1; i >= 0; i--) {
								if (getListView().isItemChecked(i)) {
									noteWarehouse.removeNote(adapter.getItem(i));
								}
							}
							mode.finish();
							adapter.notifyDataSetChanged();
							return true;
						default:
							return false;
					}
				}
				
				@Override
				public void onItemCheckedStateChanged(ActionMode mode, int position,
						long id, boolean checked) {
					// TODO Auto-generated method stub
				}
			});
		}
	}
	
	/**
	 * Generate, add new {@link Note} to the (@link NoteWarehouse}
	 * and initiate the {@link NotePagerActivity} to start
	 * editing.
	 */
	private void addNewNote() {
		// Update model
		Note note = new Note();
		NoteWarehouse.get(getActivity()).addNote(note);
		
		// Start detail note view to edit
		startNoteActivity(note);
	}
	
	/**
	 * Start {@link NotePagerActivity} starting from
	 * the respective {@link Note} fragment.
	 * @param note
	 */
	private void startNoteActivity(Note note) {
		UUID noteId = note.getId();
		Intent i = new Intent(getActivity(), NotePagerActivity.class);
		i.putExtra(NoteFragment.EXTRA_NOTE_ID, noteId);
		startActivityForResult(i, 0);
	}
	
	/*
	 * Inner Class
	 */

	private class NoteListAdapter extends ArrayAdapter<Note> {
		public NoteListAdapter (ArrayList<Note> notes) {
			// Construct custom layout with notes as its data.
			super(getActivity(), 0, notes); 
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater()
								.inflate(R.layout.list_item_note, null);
			}
			
			// Set the view with the note's data
			Note note = getItem(position);
			
			TextView titleTextView = (TextView)convertView.findViewById(R.id.title_item_textView);
			String title = note.getTitle();
			if (note.getTitle() == null || note.getTitle().length() == 0 
					|| note.getTitle().length() > MAX_LENGTH_TITLE_PREVIEW) {
				title = getConciseString(title, MAX_LENGTH_TITLE_PREVIEW);
			}
			titleTextView.setText(title);
			
			TextView summaryTextView = (TextView)convertView.findViewById(R.id.summary_item_textView);
			String summary = getConciseString(note.getContent(), MAX_LENGTH_SUMMARY_PREVIEW);
			summaryTextView.setText(summary);
			
			TextView dateTextView = (TextView)convertView.findViewById(R.id.date_item_textView);
			dateTextView.setText(note.getDate().toString());
			
			convertView.setBackgroundResource(getColorResId(note.getColor()));
			
			return convertView;
		}
	
		/**
		 * To cut the string down to maximum of
		 * maxShownChars number of characters.
		 * @param sentence
		 * @param maxShownChars
		 * @return
		 */
		private String getConciseString (String sentence, int maxShownChars) {
			String concise = sentence;
			if (concise != null && concise.trim().length() > 0) {
				concise = concise.replaceAll("\n", " ");
				int end = (concise.trim().length() >= maxShownChars)? maxShownChars : concise.trim().length();
				concise = concise.trim().substring(0, end);
			}
			else {
				concise = "";
			}
			concise += "...";
			
			return concise;
		}
		
		/**
		 * @param color
		 * @return
		 * The drawable resource ID for the 
		 * corresponding color enumerated value.
		 */
		private int getColorResId(Note.Color color) {
			int colorResId;
			switch (color) {
				case PINK:
					colorResId = R.drawable.pink_light;
					break;
				case YELLOW:
					colorResId = R.drawable.yellow_light;
					break;
				case GREEN:
					colorResId = R.drawable.green_light;
					break;
				default:
					colorResId = R.drawable.yellow_light;
					break;
			}
			return colorResId;
		}
	}
}
