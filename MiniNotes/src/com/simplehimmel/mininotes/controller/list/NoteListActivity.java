package com.simplehimmel.mininotes.controller.list;

import android.support.v4.app.Fragment;

import com.simplehimmel.mininotes.controller.share.SingleFragmentActivity;

public class NoteListActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new NoteListFragment();
	}

}
