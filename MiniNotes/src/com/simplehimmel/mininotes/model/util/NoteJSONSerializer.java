package com.simplehimmel.mininotes.model.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.simplehimmel.mininotes.model.Note;

/**
 * The object which directly interface
 * with the filesystem for saving and loading
 * a list of {@link Note}s.
 */
public class NoteJSONSerializer {
	private final static String TAG = "NoteJSONSerializer";
	private final static String APP_DIR_NAME = "himmel";
	
	private Context mContext;
	private String mFilename;
	
	public NoteJSONSerializer (Context c, String f) {
		mContext = c;
		mFilename = f;
	}
	
	/**
	 * Saving a list of {@link Note}s into
	 * a JSON formatted file.
	 * @param notes
	 * @throws IOException
	 * @throws JSONException
	 */
	public void saveNotes (ArrayList<Note> notes) 
			throws IOException, JSONException 
	{
		JSONArray array = new JSONArray();
		for (Note note : notes) {
			array.put(note.toJSON());
		}
		
		Writer writer = null;
		try {
			String filename = mFilename;
			
			boolean isFromSandbox = true;
			// Check for the existence of external storage
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				// Prepare for the external storage file location
				File externalDirectory = Environment.getExternalStorageDirectory();
				File appDir = new File(externalDirectory.getAbsolutePath() + File.separator + APP_DIR_NAME);
				if (appDir.exists() == false) {
					appDir.mkdir();
				}
				
				filename = appDir.getAbsolutePath() + File.separator + mFilename;
				isFromSandbox = false;
			}
			
			OutputStream out;
			if (isFromSandbox) {
				// Apps sandbox file area
				out = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
				Log.d(TAG, "try to save notes to external storage.");
			}
			else {
				out = new FileOutputStream(filename);
				Log.d(TAG, "try to save notes to private storage.");
			}
			
			writer = new OutputStreamWriter(out);
			writer.write(array.toString());
		} finally {
			if (writer != null)
				writer.close();
		}
	}
	
	/**
	 * Read a file and return a {@link String}
	 * representation of it.
	 * @param in
	 * @return
	 * {@link String} representation of the file.
	 * @throws IOException
	 */
	private String getStringVersion (InputStream in) throws IOException {
		String str = null;
		
		BufferedReader reader = null;
		try {
			
			reader = new BufferedReader(new InputStreamReader(in));
			
			StringBuilder strBuilder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				strBuilder.append(line);
			}
			
			str = strBuilder.toString();
		} catch (FileNotFoundException e) {
			// fresh apps will go here
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		
		return str;
	}
	
	/**
	 * Loading the {@link Note}s from a file into
	 * an {@link ArrayList} of Notes.
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public ArrayList<Note> loadNotes () throws IOException, JSONException {
		ArrayList<Note> notes = new ArrayList<Note>();
		
		BufferedReader reader = null;
		try {
			String filename = mFilename;
			
			boolean isFromSandbox = true;
			// Check for the existence of external storage
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				// Prepare for the external storage file location
				File externalDirectory = Environment.getExternalStorageDirectory();
				File appDir = new File(externalDirectory.getAbsolutePath() + File.separator + APP_DIR_NAME);
				filename = appDir.getAbsolutePath() + File.separator + mFilename;
				File file = new File(filename);
				
				isFromSandbox = false;
				if (file.exists() == false) {
					// if it was not exist yet, try to check for the sandbox area
					// this is to keep save the data of previous version of mini notes
					String sandboxFileContent = getStringVersion(mContext.openFileInput(mFilename));
					if (sandboxFileContent != null) {
						filename = mFilename;
						isFromSandbox = true;
					}
				}
			}
			
			InputStream in;
			
			if (isFromSandbox){ // Apps sandbox file area
				in = mContext.openFileInput(filename);
				Log.d(TAG, "try to load notes from external storage");
			}
			else {
				in = new FileInputStream(filename);
				Log.d(TAG, "try to load notes from private storage");
			}
			reader = new BufferedReader(new InputStreamReader(in));
			
			StringBuilder jsonString = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				jsonString.append(line);
			}
			
			JSONArray array = (JSONArray)new JSONTokener(jsonString.toString())
												.nextValue();
			for (int i = 0; i < array.length(); i++) {
				notes.add(new Note(array.getJSONObject(i)));
			}
		} catch (FileNotFoundException e) {
			// fresh apps will go here
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		
		return notes;
	}
}

