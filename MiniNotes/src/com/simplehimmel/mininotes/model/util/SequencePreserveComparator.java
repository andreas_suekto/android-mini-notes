package com.simplehimmel.mininotes.model.util;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Special {@link Comparator} which is meant
 * to make {@link PriorityQueue} to preserve its
 * insertion sequence.
 * 
 * Works alongside with {@link SequencePreserveObject}
 * to care about the previous insertion position
 * @param <T>
 */
public class SequencePreserveComparator<T> 
				implements Comparator<SequencePreserveObject<T>>{
	private Comparator<T> mBaseComparator;	
	
	public SequencePreserveComparator (Comparator<T> baseComparator) {
		mBaseComparator = baseComparator;
	}
	
	@Override
	public int compare(SequencePreserveObject<T> lhs
							, SequencePreserveObject<T> rhs) {
		int res = 0;
		
		res = mBaseComparator.compare(lhs.getBaseObject(), rhs.getBaseObject());
		if (res == 0) {
			// Use the insertion position data to maintain their
			// relative insertion sequence.
			return (lhs.getInsertTime() - rhs.getInsertTime());
		}
		
		return res;
	}
}
