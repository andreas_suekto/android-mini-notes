package com.simplehimmel.mininotes.model.util;

import java.util.PriorityQueue;

/**
 * Wrapper for an Object to be attached
 * with their insertionPosition data;
 * mainly will be used alongside {@link SequencePreserveComparator}
 * to let the {@link PriorityQueue} to preserve insertion
 * sequence
 * @param <T>
 */
public class SequencePreserveObject<T> {
	private T mBaseObject;
	private int mInsertTime;
	
	public SequencePreserveObject (T baseObject, int insertTime) {
		mBaseObject = baseObject;
		mInsertTime = insertTime;
	}

	public T getBaseObject() {
		return mBaseObject;
	}

	public int getInsertTime() {
		return mInsertTime;
	}
}
