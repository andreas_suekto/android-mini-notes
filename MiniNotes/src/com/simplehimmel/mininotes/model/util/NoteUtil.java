package com.simplehimmel.mininotes.model.util;

import java.util.Comparator;

import com.simplehimmel.mininotes.model.Note;

/**
 * Hold various utility for {@link Note}s, such
 * as the {@link Comparator}s for sorting the notes.
 */
public class NoteUtil {
	public final static boolean SORT_ASCENDING = true;
	public final static boolean SORT_DESCENDING = false;
	
	public static class ColorComparator implements Comparator<Note>{
		private Note.Color mPrioritizedColor;
		
		public ColorComparator (Note.Color prioritizedColor) {
			mPrioritizedColor = prioritizedColor;
		}
		
		/**
		 * For setting up default color priority level.
		 * @param color
		 * @return
		 */
		private int getPriorityFor(Note.Color color) {
			int priority = 0;
			
			switch (color) {
				case PINK :
					priority = 1;
					break;
				case GREEN :
					priority = 2;
					break;
				case YELLOW :
					priority = 3;
					break;
				default :
					priority = 0;
					break;
			}
			
			return priority;
		}
		
		@Override
		public int compare(Note lhs, Note rhs) {
			int res = 0;
			if (lhs.getColor().equals(mPrioritizedColor) 
					&& !(rhs.getColor().equals(mPrioritizedColor)))
			{
				res = -1;
			}
			else if (!(lhs.getColor().equals(mPrioritizedColor)) 
						&& rhs.getColor().equals(mPrioritizedColor))
			{
				res = 1;
			}
			else {
				// Finally check the default priority
				// for determining their relative priority
				res = getPriorityFor(rhs.getColor()) 
						- getPriorityFor(lhs.getColor());
			}
			
			return res;
		}
	}
	
	public static class DateComparator implements Comparator<Note>{
		private boolean mIsAscending;
		
		public DateComparator(boolean isAscending) {
			mIsAscending = isAscending;
		}
		
		@Override
		public int compare(Note lhs, Note rhs) {
			long diff = lhs.getDate().getTime() - rhs.getDate().getTime();
			if (mIsAscending == SORT_DESCENDING) {
				diff *= -1;
			}
			int res = 0;
			if (diff > 0)
				res = 1;
			else
				res = -1;
			return res;
		}
	}
	
	public static class TitleComparator implements Comparator<Note>{
		private boolean mIsAscending;
		
		public TitleComparator (boolean isAscending) {
			mIsAscending = isAscending;
		}
		
		@Override
		public int compare(Note lhs, Note rhs) {
			int res = lhs.getTitle().compareToIgnoreCase(rhs.getTitle());
			if (mIsAscending == SORT_DESCENDING) {
				res *= -1;
			}
			return res;
		}
	}
	
}
