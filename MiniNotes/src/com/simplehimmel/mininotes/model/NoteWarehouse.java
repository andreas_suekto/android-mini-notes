package com.simplehimmel.mininotes.model; 

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

import com.simplehimmel.mininotes.model.util.NoteJSONSerializer;
import com.simplehimmel.mininotes.model.util.NoteUtil;
import com.simplehimmel.mininotes.model.util.SequencePreserveComparator;
import com.simplehimmel.mininotes.model.util.SequencePreserveObject;

/**
 * A Collection of {@link Note}s, maintaining
 * them by using Singleton Design Pattern,
 * and also provide file conversion, and
 * sorting activities for the list
 * of {@link Note}s.
 */
public class NoteWarehouse {
	private static String FILENAME = "notes.json";
	private static String TAG = "NoteWarehouse";
	
	private static NoteWarehouse sNoteWarehouse;
	
	private boolean mSortDateAscendingMode;
	private Note.Color mSortPrioritizedColor;
	
	public Note.Color getSortPrioritizedColor() {
		return mSortPrioritizedColor;
	}

	private ArrayList<Note> mNotes;
	private Context mAppContext;
	private NoteJSONSerializer mSerializer;
	
	public boolean isSortDateAscendingMode() {
		return mSortDateAscendingMode;
	}

	private NoteWarehouse (Context appContext) {
		mNotes = new ArrayList<Note>();
		mAppContext = appContext;
		mSerializer = new NoteJSONSerializer(mAppContext, FILENAME);
		
		try {
			mNotes = mSerializer.loadNotes();
			Log.d(TAG, "loaded notes");
		} catch (Exception e) {
			mNotes = new ArrayList<Note>();
			Log.e(TAG, "error loading notes.", e);
		}
		
		mSortDateAscendingMode = NoteUtil.SORT_DESCENDING;
		mSortPrioritizedColor = null;
		
		if (mNotes.size() >= 1) {
			sortNote();			
		}

	}
	
	// Singleton, making sure there is only 1 warehouse at a time
	public static NoteWarehouse get(Context c) {
		if (sNoteWarehouse == null) {
			sNoteWarehouse = new NoteWarehouse(c.getApplicationContext());
		}
		
		return sNoteWarehouse;
	}
	
	public void addNote (Note note) {
		mNotes.add(note);
		sortNote();
	}
	
	public void removeNote (Note note) {
		mNotes.remove(note);
	}
	
	public Note getNote (UUID id) {
		for (Note note : mNotes) {
			if (note.getId().equals(id)) {
				return note;
			}
		}
		return null;
	}

	public ArrayList<Note> getNotes () {
		return mNotes;
	}
	
	/**
	 * In every addition / modification
	 * of {@link Note}, the {@link NoteWarehouse} should
	 * sorts it {@link Note}s again.
	 */
	public void notifyDataSetChange () {
		sortNote();
	}
	
	/**
	 * Sorting the {@link Note} based on color
	 * while maintaining the date sorting mode
	 * within the color group itself.
	 * @param prioritizeColor
	 * @return
	 * Group the {@link Note}s with the prioritizeColor
	 * at the top of the list.
	 */
	public ArrayList<Note> sortNoteByColor (Note.Color prioritizeColor) {
		mSortPrioritizedColor = prioritizeColor;
		sortNote();
		return mNotes;
	}
	
	/**
	 * Sort the {@link Note} based on the ascending mode
	 * @param ascendingSort
	 * @return
	 * sorted {@link Note} by modification date either in ascending
	 * or descending.
	 */
	public ArrayList<Note> sortNoteByDate (boolean ascendingSort) {
		mSortDateAscendingMode = ascendingSort;
		sortNote();
		return mNotes;
	}
	
	/**
	 * @return
	 * Sort the {@link Note} with the current {@link Date}
	 * and Color setting. Date sorting sequence
	 * is preserved per color group.
	 */
	private ArrayList<Note> sortNote () {
		sortNoteByDateExclusive();
		sortNoteByColorExclusive();
		return mNotes;
	}
	
	/**
	 * Sorting {@link Note}s by color without regards
	 * of anything else
	 * @return
	 */
	private ArrayList<Note> sortNoteByColorExclusive () {
		if (mSortPrioritizedColor == null)
			return mNotes;
		
		Comparator<Note> comparator 
			= new NoteUtil.ColorComparator(mSortPrioritizedColor);
		
		return sortNoteBy(comparator);
	}
	
	/**
	 * Sorting {@link Note}s by date without regards of
	 * anything else
	 * @return
	 */
	private ArrayList<Note> sortNoteByDateExclusive () {
		Comparator<Note> comparator;
		if (mSortDateAscendingMode) {
			comparator = new NoteUtil.DateComparator(
								NoteUtil.SORT_ASCENDING);
		}
		else {
			comparator = new NoteUtil.DateComparator(
								NoteUtil.SORT_DESCENDING);
		}
		
		return sortNoteBy(comparator);
	}
	
	/**
	 * Generic sorter based on a comparator
	 * A set of comparator is provided in
	 * {@link NoteUtil}
	 * @param comparator
	 * @return
	 */
	private ArrayList<Note> sortNoteBy (Comparator<Note> comparator) {
		if (mNotes.size() >= 1) {
			PriorityQueue<SequencePreserveObject<Note>> queue 
					= new PriorityQueue<>(mNotes.size()
						, new SequencePreserveComparator<Note>(comparator));
			for (int i = 0; i < mNotes.size(); i++) {
				queue.add(new SequencePreserveObject<Note>(mNotes.get(i), i));
			}
			mNotes.clear();
			
			while (true) {
				SequencePreserveObject<Note> preserveNote = queue.poll();
				if (preserveNote == null)
					break;
				Note note = preserveNote.getBaseObject();
				mNotes.add(note);
			}
		}
		
		return mNotes;
	}
	
	/*
	 * File related functions
	 */
	
	/**
	 * Initiating a save to a file
	 * by using a correct serializer.
	 * @return
	 * Whether the save is successful or not.
	 */
	public boolean save() {
		try {
			mSerializer.saveNotes(mNotes);
			Log.d(TAG, "notes saved to file.");
			return true;
		} catch (Exception e) {
			Log.e(TAG, "failed to save notes:", e);
		} 
		
		return false;
	}
}
