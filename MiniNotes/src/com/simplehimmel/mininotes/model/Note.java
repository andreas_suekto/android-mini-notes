package com.simplehimmel.mininotes.model;

import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

public class Note {
	public static final Note EMPTY_NOTE = new Note();
	
	private static final String JSON_ID ="id";
	private static final String JSON_TITLE="title";
	private static final String JSON_CONTENT="content";
	private static final String JSON_DATE="date";
	private static final String JSON_COLOR="color";
	
	public enum Color {
		YELLOW, PINK, GREEN;
		
		public static Color getColor(String color) {
			switch (color) {
				case "yellow": 
					return YELLOW;
				case "pink":
					return PINK;
				case "green":
					return GREEN;
				default:
					return YELLOW;
			}
		}
		
		@Override
		public String toString() {
			switch (this) {
				case YELLOW: 
					return "yellow";
				case PINK:
					return "pink";
				case GREEN:
					return "green";
				default:
					return "unknown";
			}
		}
	}
	
	private String mOldTitle;
	private String mOldContent;
	private Color mOldColor;
	
	private UUID mId;
	private String mTitle;
	private Date mDate; 
	private String mContent;
	private Color mColor;
	
	public Note () {
		mId = UUID.randomUUID();
		mDate = new Date();
		mColor = Color.YELLOW;
	}
	
	/**
	 * Creating a {@link Note} based on a {@link JSONObject}.
	 * Mainly use to load the Object from a file.
	 * @param json
	 * @throws JSONException
	 */
	public Note (JSONObject json) throws JSONException {
		mId = UUID.fromString(json.getString(JSON_ID));
		if (json.has(JSON_TITLE)) {
			mTitle = json.getString(JSON_TITLE);
		}
		if (json.has(JSON_CONTENT)) {
			mContent = json.getString(JSON_CONTENT);
		}
		mColor = Color.getColor(json.getString(JSON_COLOR));
		mDate = new Date(json.getLong(JSON_DATE));
		
		synchOldValues();
	}
	
	public UUID getId() {
		return mId;
	}

	public void setId(UUID id) {
		mId = id;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		mTitle = title;
		notifyChange();
	}

	public Date getDate() {
		return mDate;
	}

	public void setDate(Date date) {
		mDate = date;
	}

	public String getContent() {
		return mContent;
	}

	public void setContent(String content) {
		mContent = content;
		notifyChange();
	}

	public Color getColor() {
		return mColor;
	}

	public void setColor(Color color) {
		this.mColor = color;
		notifyChange();
	}
	
	/**
	 * Monitoring possible changes, and
	 * update the modification {@link Date} accordingly
	 */
	private void notifyChange() {
		boolean isTitleChanged = false;
		if (mOldTitle == null) {
			if (mTitle != null)
				isTitleChanged = true;
		}
		else {
			isTitleChanged = !mOldTitle.equals(mTitle);
		}
		
		boolean isContentChanged = false;
		if (mOldContent == null) {
			if (mContent != null)
				isContentChanged = true;
		}
		else {
			isContentChanged = !mOldContent.equals(mContent);
		}
		
		if (mOldColor != mColor || isTitleChanged || isContentChanged) {
			//Update modification date
			mDate = new Date();
		}
		synchOldValues();
	}
	
	private void synchOldValues() {
		mOldTitle = mTitle;
		mOldColor = mColor;
		mOldContent = mContent;
	}
	
	/*
	 * File Related Constructor and Function
	 */
	
	/**
	 * Converting the {@link Note} into a {@link JSONObject}
	 * mainly used for saving to a file.
	 * @return
	 * {@link JSONObject} representation of the {@link Note}
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();
		
		json.put(JSON_ID, mId.toString());
		json.put(JSON_TITLE, mTitle);
		json.put(JSON_CONTENT, mContent);
		json.put(JSON_DATE, mDate.getTime());
		json.put(JSON_COLOR, mColor.toString());
		
		return json;
	}
}
